import unittest
from main import *


class MyTestCase(unittest.TestCase):
    def testchiffrement(self):
        self.assertEqual(chiffrement("aaaa",2), "cccc")
        self.assertEqual(chiffrement("Yoyo", 1), "Zpzp")
        self.assertNotEqual(chiffrement("abc", 3), "ef")

    def testdechiffrement(self):
        self.assertEqual(dechiffrement("cccc",2), "aaaa")
        self.assertEqual(dechiffrement("Zpzp", 1), "Yoyo")
        self.assertNotEqual(dechiffrement("ef", 3), "abc")
        
    def testsaisiecorrect(self):
        self.assertEqual(entrecorrect("111"),True)
        self.assertEqual(entrecorrect("Azerty"),False)

if __name__ == '__main__':
    unittest.main()
