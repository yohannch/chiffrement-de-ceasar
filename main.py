from tkinter import *

def gen_alphabet(key):
    alphabet = {}
    for indice in range(26):  ##pour chaque nmbre de 0 jusqu'a 25
        indice_circulaire = (indice + key) % 26

        char_decale = chr(indice_circulaire + 97)
        alph_index = chr(indice + 97)
        alphabet[alph_index] = char_decale

    return alphabet




def chiffrement(chaine, key):
    if getvalcheck() == True:
        alphabet = gen_alphabet(key)
        nch = ""
        for index in chaine:
            nch += alphabet[index]
    else:
        nch = ""
        for i in chaine:
            nch += chr(ord(i) + key)
        return nch

    return nch

def affichage_chiffrement():
    textarea2.configure(state="normal")
    chaine_entree = textarea.get("1.0", 'end-1c')
    textarea.delete("1.0", 'end-1c')
    textarea2.delete("1.0", 'end-1c')
    try:
        chaine_sortie = chiffrement(chaine_entree, int(saisie.get()))
        textarea2.insert(END, "{}".format(chaine_sortie))
    except:
        textarea2.insert(END, "Clé invalide, veuillez saisir un entier")
    textarea2.configure(state="disabled")




def dechiffrement(chaine, key):
    if getvalcheck()==True:
        alphabet = gen_alphabet(key)
        nch = ""
        for index in chaine:
            nch += alphabet[index]
    else:
        nch = ""
        for i in chaine:
            nch += chr(ord(i) - key)
        return nch


def affichage_dechiffrement():
    textarea2.configure(state="normal")
    chaine_entree = textarea.get("1.0", 'end-1c')
    textarea.delete("1.0", 'end-1c')
    textarea2.delete("1.0", 'end-1c')
    try:
        chaine_sortie = dechiffrement(chaine_entree, int(saisie.get()))
        textarea2.insert(END, "{}".format(chaine_sortie))
    except:
        textarea2.insert(END, "Clé invalide, veuillez saisir un entier")
    textarea2.configure(state="disabled")


def getvalcheck():
    if value_cb.get() == 1:
        res=True
    else:
        res=False
    return res




fenetre = Tk()


head_frame = Frame(fenetre)
head_frame.grid(row=0, column=0)


lab = Label(head_frame, text="Saisissez votre texte")
lab.config(justify='center',font=("Arial", 32))
lab.pack(side="top")


value_cb=IntVar()
cb_circulaire = Checkbutton(head_frame, variable=value_cb, text="circulaire")
cb_circulaire.pack()

frame = Frame(fenetre)
frame.grid(row=2, column=0)

textarea = Text(fenetre, width="75", height="7")
textarea.grid(row=1, column=0, padx=20, pady=10, ipady=5)
textarea2 = Text(fenetre, width="75", height="7")
textarea2.grid(row=3, column=0, padx=20, pady=10, ipady=5)

saisie = Entry(frame, width=15, text="Saisir clé de chiffrement")
saisie.pack(side="bottom")





lab_key = Label(frame, text="Clé de chiffrement:")
bouton_chiffrement = Button(frame, width="10", text="Chiffrer", font=("Arial", 20))
bouton_dechiffrement = Button(frame, width="10", text="Déchiffrer", font=("Arial", 20))
bouton_chiffrement.pack(side="left")
bouton_dechiffrement.pack(side="right")
lab_key.pack(side="bottom")



fenetre.geometry("850x650")
textarea.configure(bg="black", fg="green")
textarea2.configure(bg="black", fg="green",state="disabled")
saisie.configure(bg="black", fg="green")
bouton_chiffrement.configure(command=affichage_chiffrement)
bouton_dechiffrement.configure(command=affichage_dechiffrement)

fenetre.mainloop()